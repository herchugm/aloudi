﻿using Aloudi.Web.Data.Contracts;
using Aloudi.Web.Data.Models;
using Aloudi.Web.Infra.Metadata;
using Aloudi.Web.Models;
using Aloudi.Web.Models.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Aloudi.Web.Controllers
{
    [Route("{action:alpha=Index}")]
    public partial class HomeController : Controller
    {
        private ISectionRepository SectionRepository { get; set; }
        private IIndustryRepository IndustryRepository { get; set; }
        //private WebsiteInfo WebsiteInfo { get; set; }

        public HomeController
        (
              ISectionRepository sectionRepository
            , IIndustryRepository industryRepository
        )
        {
            //this.WebsiteInfo = new WebsiteInfo();
            this.SectionRepository = sectionRepository;
            this.IndustryRepository = industryRepository;
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            return View(this.SectionRepository.GetSections());
        }

        #region -- Robots() Method --
        [HttpGet, Route("~/robots.txt")]
        public virtual ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            return View();
        }
        #endregion

        [ChildActionOnly]
        public virtual ActionResult Section(string section)
        {
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView(section);
            }
            return View(section);
        }

        [ChildActionOnly]
        public virtual PartialViewResult Usos()
        {
            return PartialView(IndustryRepository.GetIndustries());
        }

        [ChildActionOnly]
        public virtual PartialViewResult Sidebar()
        {
            return PartialView("_Sidebar", SectionRepository.GetMenuItems());
        }

        [ChildActionOnly]
        public virtual PartialViewResult Header()
        {
            var websiteInfo = new WebsiteInfo();
            var model = new HeaderViewModel()
            {
                CompanyName = websiteInfo.CompanyName,
                Menu = SectionRepository.GetMenuItems()
            };
            return PartialView("_Header", model);
        }

        [ChildActionOnly]
        public virtual ContentResult Title()
        {
            var websiteInfo = new WebsiteInfo();
            return new ContentResult() { Content = websiteInfo.CompanyName + " | " + websiteInfo.Tagline };
        }

        [ChildActionOnly]
        public virtual ContentResult GetMetadata()
        {
            var sb = new StringBuilder();
            var websiteInfo = new WebsiteInfo();
            sb.AppendLine(Mapper.Map<MetadataInfo>(websiteInfo).ToString());
            sb.AppendLine(Mapper.Map<FacebookMetadata>(websiteInfo).ToString());
            sb.AppendLine(Mapper.Map<TwitterMetadata>(websiteInfo).ToString());
            sb.AppendLine(Mapper.Map<DublinCoreMetadata>(websiteInfo).ToString());
            return new ContentResult() { Content = sb.ToString() };
        }

        [ChildActionOnly]
        public virtual PartialViewResult Footer()
        {
            var websiteInfo = new WebsiteInfo();
            var model = new FooterViewModel()
            {
                Menu = SectionRepository.GetMenuItems(),
                Tags = websiteInfo.FooterKeywords,
                Copyright = websiteInfo.Copyright,
                Description = websiteInfo.Description
            };
            return PartialView("_Footer", model);
        }
    }
}