﻿using System.Web.Mvc;

namespace Aloudi.Web.Areas.Dummy
{
    //este archivo se agrega para arreglar un problema de T4MVC
    public class DummyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Dummy";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Dummy_default",
                "Dummy/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}