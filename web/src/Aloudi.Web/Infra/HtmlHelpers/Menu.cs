﻿using Aloudi.Web.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public class Menu : IHtmlString
    {
        private TagBuilder TagBuilder { get; set; }

        private IEnumerable<Section> MenuItems { get; set; }

        public Menu(IEnumerable<Section> menuItems)
        {
            this.TagBuilder = new TagBuilder("ul");
            this.MenuItems = menuItems;
        }

        public Menu WithId(string id)
        {
            this.TagBuilder.GenerateId(id);
            return this;
        }

        public Menu WithClass(string value)
        {
            this.TagBuilder.AddCssClass(value);
            return this;
        }

        public string ToHtmlString()
        {
            if (this.MenuItems == null || this.MenuItems.Count() == 0)
            {
                return string.Empty;
            }
            foreach (var mi in this.MenuItems)
            {
                var a = new TagBuilder("a");
                a.Attributes.Add("href", string.Format("#{0}", mi.Action.ToLower()));
                a.InnerHtml = mi.Label;
                var li = new TagBuilder("li");
                li.InnerHtml = a.ToString();
                this.TagBuilder.InnerHtml += li.ToString();
            }
            return this.TagBuilder.ToString();
        }

        public override string ToString()
        {
            return this.ToHtmlString();
        }
    }

    public static class MenuExtensions
    {

        public static Menu Menu(this HtmlHelper helper, IEnumerable<Section> menuItems)
        {
            return new Menu(menuItems);
        }
    }
}