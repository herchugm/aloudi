﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class UrlHelperExtensions
    {

        public static string ContentFullPath(this UrlHelper url, string virtualPath)
        {
            return url.Domain() + VirtualPathUtility.ToAbsolute(virtualPath);
        }

        public static string Domain(this UrlHelper url)
        {
            Uri requestUrl = url.RequestContext.HttpContext.Request.Url;
            return string.Format("{0}://{1}", requestUrl.Scheme, requestUrl.Authority);
        }
    }
}