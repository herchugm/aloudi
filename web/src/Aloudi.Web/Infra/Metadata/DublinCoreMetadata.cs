﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aloudi.Web.Infra.Metadata
{
    public class DublinCoreMetadata : MetadataBaseTags
    {
        public string Language { get; set; }
        public string Source { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        //public string Date { get; set; }
        public string Rights { get; set; }

        public DublinCoreMetadata()
            : base("dc", "name", ".")
        { }
    }
}