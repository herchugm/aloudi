﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Aloudi.Web.Infra.Metadata
{
    public class TwitterMetadata : MetadataBaseTags
    {
        public string Card { get; private set; }
        public string Site { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image_Src { get; set; }
        public string Domain { get; set; }

        public TwitterMetadata()
            : base("twitter", "name", ":")
        {
            this.Card = "summary";
        }

    }
}