﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Aloudi.Web.Infra.Metadata
{
    public abstract class MetadataBaseTags
    {
        private string PATTERN = "<meta {0}=\"{1}\" content=\"{2}\">";
        protected string Id { get; private set; }
        protected string AttributeName { get; private set; }
        private string Delimiter { get; set; }

        protected MetadataBaseTags(string id, string attributeName, string delimiter)
        {
            this.Id = !string.IsNullOrEmpty(id) ? id.ToLower() : string.Empty;
            this.AttributeName = attributeName;
            this.Delimiter = delimiter;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var prop in this.GetType().GetProperties())
            {
                var value = (string)prop.GetValue(this);
                if (!string.IsNullOrEmpty(value))
                {
                    sb.AppendLine(string.Format(PATTERN, this.AttributeName, CreateName(prop), value));
                }
            }
            return sb.ToString();
        }

        private string CreateName(PropertyInfo prop)
        {
            var list = new List<string>();
            if (!string.IsNullOrEmpty(Id))
            {
                list.Add(Id);
            }
            list.Add(prop.Name.Replace("_", Delimiter).ToLower());
            return string.Join(Delimiter, list);
        }
    }
}