﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aloudi.Web.Infra.Metadata
{
    public class MetadataInfo : MetadataBaseTags
    {
        public MetadataInfo()
            : base(null, "name", ".") { }

        public string Description { get; set; }
        //public string Copyright { get; set; }
        public string Keywords { get; set; }
    }
}