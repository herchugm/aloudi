﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Aloudi.Web.Infra.Metadata
{
    public class FacebookMetadata : MetadataBaseTags
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string Site_name { get; set; }

        protected FacebookMetadata()
            : base("og", "property", ":")
        {
            this.Type = "website";
        }
    }
}