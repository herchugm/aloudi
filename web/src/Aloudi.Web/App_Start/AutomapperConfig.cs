﻿using Aloudi.Web.Infra.Metadata;
using Aloudi.Web.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aloudi.Web.App_Start
{
    public static class AutomapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<WebsiteInfo, TwitterMetadata>()
                .ForMember(dest => dest.Image_Src, opts => opts.MapFrom(src => DependencyResolver.Current.GetService<UrlHelper>().ContentFullPath(src.Logo)))
                .ForMember(dest => dest.Site, opts => opts.MapFrom(src => src.TwitterHandle));

            Mapper.CreateMap<WebsiteInfo, FacebookMetadata>()
                .ForMember(dest => dest.Site_name, opts => opts.MapFrom(src => src.Title))
                .ForMember(dest => dest.Image, opts => opts.MapFrom(src => DependencyResolver.Current.GetService<UrlHelper>().ContentFullPath(src.Logo)))
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => DependencyResolver.Current.GetService<UrlHelper>().Domain()))
                .ForMember(dest => dest.Type, opts => opts.Ignore());

            Mapper.CreateMap<WebsiteInfo, MetadataInfo>()
                .ForMember(dest => dest.Keywords, opts => opts.MapFrom(src => string.Join(",", src.Keywords)));

            Mapper.CreateMap<WebsiteInfo, DublinCoreMetadata>()
                .ForMember(dest=> dest.Rights, opts=> opts.MapFrom(src=> src.Copyright))
                .ForMember(dest => dest.Language, opts => opts.MapFrom(src => System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName))
                .ForMember(dest => dest.Source, opts => opts.MapFrom(src => DependencyResolver.Current.GetService<UrlHelper>().Domain()))
                .ForMember(dest => dest.Keywords, opts => opts.MapFrom(src => string.Join(",", src.Keywords)));

        }
    }
}
