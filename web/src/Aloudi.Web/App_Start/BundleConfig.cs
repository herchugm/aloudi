﻿using System.Web;
using System.Web.Optimization;

namespace Aloudi.Web
{
    public class BundleConfig
    {
        private static string BUNDLE_PATH_PATTERN { get { return "~/bundles/{0}"; } }

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterCssBundles(bundles);
            RegisterScriptBundles(bundles);
        }



        private static void RegisterCssBundles(BundleCollection bundles)
        {
            bundles.Add(
                new LessBundle(CreateBundleVirtualPath("css"))
                .Include("~/Content/less/bootstrap/bootstrap.less")
                .Include("~/Content/css/*.less")
                .Include("~/Content/css/owl.carousel.css", "~/Content/css/owl.theme.css")
                .Include("~/Content/css/style.css")
                .Include("~/Content/css/fonticons.css")
            );
        }



        private static void RegisterScriptBundles(BundleCollection bundles)
        {
            bundles.Add(
                new ScriptBundle(CreateBundleVirtualPath("jquery"))
                .Include("~/Content/js/jquery-{version}.js")
            );

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(
                new ScriptBundle(CreateBundleVirtualPath("modernizr"))
                .Include("~/Content/js/modernizr-*")
            );

            bundles.Add(
                new ScriptBundle(CreateBundleVirtualPath("bootstrap"))
                .Include("~/Content/js/bootstrap.min.js")
            );

            bundles.Add(
                new Bundle(CreateBundleVirtualPath("ie"))
                .Include("~/Content/js/html5shiv.js")
                .Include("~/Content/js/respond.min.js")
            );

            bundles.Add(
                new ScriptBundle(CreateBundleVirtualPath("js"))
                .Include("~/Content/js/owl.carousel.min.js")
                .Include("~/Content/js/jquery.easing.min.js")
                .Include("~/Content/js/classie.js")
                .Include("~/Content/js/jquery.vticker.min.js")
                .Include("~/Content/js/jquery.flexslider-min.js")
                .Include("~/Content/js/sidebarEffects.js")
                .Include("~/Content/js/jquery.easypiechart.min.js")
                .Include("~/Content/js/jquery.isotope.min.js")
                .Include("~/Content/js/jquery.countTo.js")
                .Include("~/Content/js/jquery.magnific-popup.min.js")
                .Include("~/Content/js/twitterFetcher_v10_min.js")
                .Include("~/Content/js/custom.js")
            );
            BundleTable.EnableOptimizations = false;
        }



        private static string CreateBundleVirtualPath(string name)
        {
            return string.Format(BUNDLE_PATH_PATTERN, name);
        }
    }
}
