﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aloudi.Web.Models
{
    public class WebsiteInfo
    {
        public string CompanyName { get; private set; }
        public string Title { get; private set; }
        public string Tagline { get; private set; }
        public string Description { get; private set; }
        public string Copyright { get; private set; }
        public string Logo { get; private set; }
        public List<string> Keywords { get; private set; }
        public List<string> FooterKeywords { get; private set; }
        public string TwitterHandle { get; private set; }
        public string FacebookHandle { get; private set; }
        public WebsiteInfo()
        {
            this.CompanyName = "aloudi";
            this.Tagline = "Despliega twitter en cualquier pantalla en eventos, conferencias, ferias, conciertos, y más";
            this.Logo = "~/Content/images/logo.png";
            this.Title = string.Format("{0} | {1}", this.CompanyName, this.Tagline);
            this.Description = "aloudi es una herramienta que te permite desplegar tus redes sociales en cualquier lado, de una manera controlada y visualmente atractiva.";
            this.Keywords = new List<string>() { "aloudi", "redes sociales", "twitter", "instagram", "facebook", "tweet wall", "tweetwall", "eventos", "ferias", "exposiciones", "conferencias", "congresos", "television" };
            this.FooterKeywords = new List<string>() { "Redes Sociales", "Tweetwall", "Eventos", "Social Media", "Exposiciones", "Marketing" };
            this.Copyright = string.Format("Copyright © {0}", DateTime.Now.Year.ToString());
            this.TwitterHandle = "@aloudicom";
            this.FacebookHandle = "aloudicom";
        }
        public string TwitterUrl { get { return string.Format("http://www.twitter.com/{0}", this.TwitterHandle.Replace("@", string.Empty)); } }
        public string FacebookUrl { get { return string.Format("http://www.facebook.com/{0}", this.FacebookHandle); } }
    }


}