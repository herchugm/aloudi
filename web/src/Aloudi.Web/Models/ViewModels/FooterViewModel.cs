﻿using Aloudi.Web.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aloudi.Web.Models.ViewModels
{
    public class FooterViewModel
    {
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string Description { get; set; }
        public IEnumerable<Section> Menu { get; set; }
        public List<string> Tags { get; set; }
        public string Copyright { get; set; }
    }
}