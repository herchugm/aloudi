﻿using Aloudi.Web.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aloudi.Web.Models.ViewModels
{
    public class HeaderViewModel
    {
        public string CompanyName { get; set; }
        public IEnumerable<Section> Menu { get; set; }
    }
}