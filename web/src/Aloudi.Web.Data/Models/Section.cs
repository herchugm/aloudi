using System;
using System.Collections.Generic;

namespace Aloudi.Web.Data.Models
{
    public partial class Section
    {
        public Section()
        {
            this.Section1 = new List<Section>();
        }

        public int Id { get; set; }
        public string Label { get; set; }
        public bool ShowSection { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int Order { get; set; }
        public string SectionClasses { get; set; }
        public Nullable<int> Parent { get; set; }
        public bool ShowInMenu { get; set; }
        public bool CustomProcessing { get; set; }
        public virtual ICollection<Section> Section1 { get; set; }
        public virtual Section Section2 { get; set; }
    }
}
