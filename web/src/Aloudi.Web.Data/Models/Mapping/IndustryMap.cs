using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Aloudi.Web.Data.Models.Mapping
{
    public class IndustryMap : EntityTypeConfiguration<Industry>
    {
        public IndustryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.Caption)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Imagenes)
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.Text)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Industry");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Caption).HasColumnName("Caption");
            this.Property(t => t.Imagenes).HasColumnName("Imagenes");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Show).HasColumnName("Show");
            this.Property(t => t.Order).HasColumnName("Order");
        }
    }
}
