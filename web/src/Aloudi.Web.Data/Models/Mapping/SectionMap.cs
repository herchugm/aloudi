using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Aloudi.Web.Data.Models.Mapping
{
    public class SectionMap : EntityTypeConfiguration<Section>
    {
        public SectionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Label)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Controller)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Action)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.SectionClasses)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Section");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Label).HasColumnName("Label");
            this.Property(t => t.ShowSection).HasColumnName("ShowSection");
            this.Property(t => t.Controller).HasColumnName("Controller");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.Order).HasColumnName("Order");
            this.Property(t => t.SectionClasses).HasColumnName("SectionClasses");
            this.Property(t => t.Parent).HasColumnName("Parent");
            this.Property(t => t.ShowInMenu).HasColumnName("ShowInMenu");
            this.Property(t => t.CustomProcessing).HasColumnName("CustomProcessing");

            // Relationships
            this.HasOptional(t => t.Section2)
                .WithMany(t => t.Section1)
                .HasForeignKey(d => d.Parent);

        }
    }
}
