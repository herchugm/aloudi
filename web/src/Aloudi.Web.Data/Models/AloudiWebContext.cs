using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Aloudi.Web.Data.Models.Mapping;

namespace Aloudi.Web.Data.Models
{
    public partial class AloudiWebContext : DbContext
    {
        static AloudiWebContext()
        {
            Database.SetInitializer<AloudiWebContext>(null);
        }

        public AloudiWebContext()
            : base("Name=AloudiWebContext")
        {
        }

        public DbSet<Industry> Industries { get; set; }
        public DbSet<Section> Sections { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new IndustryMap());
            modelBuilder.Configurations.Add(new SectionMap());
        }
    }
}
