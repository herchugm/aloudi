using System;
using System.Collections.Generic;

namespace Aloudi.Web.Data.Models
{
    public partial class Industry
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public string Imagenes { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool Show { get; set; }
        public int Order { get; set; }
    }
}
