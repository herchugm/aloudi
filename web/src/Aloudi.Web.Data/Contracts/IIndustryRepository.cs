﻿using Aloudi.Web.Data.Models;
using Eqip.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aloudi.Web.Data.Contracts
{
    public interface IIndustryRepository : IGenericRepository<Industry>
    {
        IEnumerable<Industry> GetIndustries();
    }
}
