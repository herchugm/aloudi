﻿using Aloudi.Web.Data.Contracts;
using Aloudi.Web.Data.Models;
using Eqip.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aloudi.Web.Data.Repositories
{
   public class IndustryRepository : GenericRepository<Industry, AloudiWebContext>, IIndustryRepository
    {

       public IndustryRepository(AloudiWebContext context)
            : base(context)
        {
        }

        public IEnumerable<Industry> GetIndustries()
        {
            return Query().Where(i => i.Show).OrderBy(i => i.Order).ToList();
        }

      
    }
}
