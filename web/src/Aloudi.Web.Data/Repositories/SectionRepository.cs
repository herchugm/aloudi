﻿using Aloudi.Web.Data.Contracts;
using Aloudi.Web.Data.Models;
using Eqip.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aloudi.Web.Data.Repositories
{
    public class SectionRepository : GenericRepository<Section, AloudiWebContext>, ISectionRepository
    {
        public SectionRepository(AloudiWebContext context)
            : base(context)
        {
        }

        public IEnumerable<Section> GetSections()
        {
            return this.Query().Where(s => s.ShowSection).OrderBy(s => s.Order).ToList();
        }



        public IEnumerable<Section> GetMenuItems()
        {
            return this.Query().Where(s => s.ShowInMenu).OrderBy(s => s.Order).ToList();
        }
    }
}
