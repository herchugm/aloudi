﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Piwer.ETL.Extractors.Twitter {
    public class QueryFilter {
        public QueryFilter() {
            this.ExactPhrases = new List<string>();
            this.AnyWord = new List<string>();
            this.NoneWord = new List<string>();
            this.HashTags = new List<string>();
            this.FromThese = new List<string>();
            this.ToThese = new List<string>();
            this.MentionThese = new List<string>();
        }
        //q:1 "2" 3 -4 #5 from:6 to:7 @8 near:"9"
        public List<string> ExactPhrases { get; set; }
        public List<string> AnyWord { get; set; }
        public List<string> NoneWord { get; set; }
        public List<string> HashTags { get; set; }
        public List<string> FromThese { get; set; }
        public List<string> ToThese { get; set; }
        public List<string> MentionThese { get; set; }
    }
}
