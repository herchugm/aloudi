﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piwer.ETL.Extractors.Twitter {
    public static class TwitterContextFactory {
        public static TwitterContext CreateTwitterContext(string consumerKey, string consumerSecret, string accessToken, string accessSecret) {
            var auth = new SingleUserAuthorizer {
                CredentialStore = new SingleUserInMemoryCredentialStore {
                    ConsumerKey = consumerKey,
                    ConsumerSecret = consumerSecret,
                    AccessToken = accessToken,
                    AccessTokenSecret = accessSecret
                }
            };

            auth.AuthorizeAsync();

            return new TwitterContext(auth);
        }
    }
}
