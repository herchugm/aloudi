﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piwer.ETL.Extractors.Twitter {
    /// <summary>
    /// Twitter user lookup
    /// </summary>
    public class TwitterLookup {

        private TwitterContext _twitterCtx;
        public TwitterLookup(TwitterContext twitterContext) {
            this._twitterCtx = twitterContext;
        }

        public async Task<List<User>> LookupUsersAsync(string[] users) {
            return await LookupUsersAsync(string.Join(",", users));
        }

        public async Task<List<User>> LookupUsersAsync(string users) {
            var userResponse =
                await
                (from user in this._twitterCtx.User
                 where user.Type == UserType.Lookup &&
                       user.ScreenNameList == users//JoeMayo,Linq2Tweeter"
                 select user)
                .ToListAsync();

            return userResponse;
        }

        public async Task<List<Status>> Search(QueryBuilder qbuilder) {
            return await Search(qbuilder.Build());
        }

        public async Task<List<Status>> Search(string query) {
            var results =
                    await
                    (from search in this._twitterCtx.Search
                     where search.Type == SearchType.Search &&
                           search.Query == query &&
                           search.Count == 10
                     select search.Statuses)
                    .ToListAsync();

            return results.SingleOrDefault();
        }
    }
}
