﻿using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Piwer.ETL.Extractors.Twitter {
    class Program {

        private const string YOUR_CONSUMER_KEY = @"cX2HsTc3fLiibuBqa6n6hmwT1";
        private const string YOUR_CONSUMER_SECRET = @"Lv0TIqtTTXQGtHwD0x730xJ51cbA8vjGM14ywi0lkdIeByelJs";

        private const string ACCESS_TOKEN = @"98174066-7eZdshBKGQiplgty7gmPWuCLxu8zIwInLA0ux1zmw";
        private const string ACCESS_TOKEN_SECRET = @"rr7tZ7zlNMd2KPLaC9zWOd28smexBdYq2zWLHUkPyLgP7";

        public static void Main() {
            var twitterCtx = TwitterContextFactory.CreateTwitterContext(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var twitter = new TwitterLookup(twitterCtx);

            ExecOnConsole(() => { StatusesWithQueryBuilder(twitter); }, "StatusesWithQueryBuilder");
            ExecOnConsole(() => { ListStatuses(twitter); }, "ListStatuses");
            ExecOnConsole(() => { ListUsers(twitter); }, "ListUsers");

            ExecOnConsole(() => { SearchRaw(twitterCtx); }, "SearchRaw");
            ExecOnConsole(() => { Search(twitterCtx); }, "Search");

            Console.ReadKey();
        }

        private static void StatusesWithQueryBuilder(TwitterLookup twitter) {
            var queryBuilder = new QueryBuilder().WithMentionThese("geferreyra");
            var statusesWithQryBuilder = twitter.Search(queryBuilder);
            foreach (var status in statusesWithQryBuilder.Result) {
                Console.WriteLine("\nName: {0}, Tweet: {1}\n", status.User.Name, status.Text);
            }
        }

        private static void ListStatuses(TwitterLookup twitter) {
            var statuses = twitter.Search("Obama");

            foreach (var status in statuses.Result) {
                Console.WriteLine("\nName: {0}, Tweet: {1}\n", status.User.Name, status.Text);
            }
        }
        private static void ListUsers(TwitterLookup twitter) {
            var listUsers = twitter.LookupUsersAsync("geferreyra,herchugm");

            foreach (var user in listUsers.Result) {
                Console.WriteLine("\nName: {0}, Descripction: {1}\n", user.Name, user.Description);
            }
        }

        private static void SearchRaw(TwitterContext twitterCtx) {
            var search = PerformSearchRawAsync(twitterCtx, "@albertomontt");
            Console.WriteLine("{0}", search.Result.Response);
        }

        static async Task<Raw> PerformSearchRawAsync(TwitterContext twitterCtx, string query) {
            string encodedStatus = Uri.EscapeDataString(query);
            string queryString = "search/tweets.json?q=" + encodedStatus;

            return await
                (from raw in twitterCtx.RawQuery
                 where raw.QueryString == queryString
                 select raw)
                .SingleOrDefaultAsync();
        }

#warning Cuando se utiliza 'from-to' aparentemente solo se muestran los tweets del día ###REVISAR
        static void Search(TwitterContext ctx) {
            var search = Search(ctx, "from:herchugm to:geferreyra");

            foreach (var status in search.Result.Statuses) {
                Console.WriteLine("\nName: {0}, Descripction: {1}\n", status.ScreenName, status.Text);
            }
        }

        static async Task<Search> Search(TwitterContext ctx, string query) {
            var search = await (
                ctx.Search.Where(s => s
                        .Type == SearchType.Search &&
                        s.Query == query
                    ).SingleOrDefaultAsync());

            return search;
        }

        private static void ExecOnConsole(Action fn, string title) {
            Console.Clear();

            Console.WriteLine("*************************");
            Console.WriteLine("***{0}***", title);
            Console.WriteLine("*************************");

            fn();

            Console.WriteLine("*************************");
            Console.WriteLine("*************************");

            Console.ReadKey();
        }
    }
}
