﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Piwer.ETL.Extractors.Twitter {
    public class QueryBuilder {
        public QueryBuilder()
            : this(new QueryFilter()) {
        }
        public QueryBuilder(QueryFilter filter) {
            this.filter = filter;
        }

        public QueryBuilder WithExactPhrase(string phrase) {
            var arg = phrase.ToLower().Trim();
            if (!this.filter.ExactPhrases.Contains(arg))
                this.filter.ExactPhrases.Add(arg);

            return this;
        }

        public QueryBuilder WithAnyWord(string word) {
            var arg = word.ToLower().Trim();
            if (!this.filter.AnyWord.Contains(arg))
                this.filter.AnyWord.Add(arg);

            return this;
        }

        public QueryBuilder WithNoneWord(string word) {
            var arg = word.ToLower().Trim();
            if (!this.filter.NoneWord.Contains(arg))
                this.filter.NoneWord.Add(arg);

            return this;
        }

        public QueryBuilder WithHashTag(string hashTag) {
            var arg = hashTag.ToLower().Trim();
            if (!this.filter.HashTags.Contains(arg))
                this.filter.HashTags.Add(arg);

            return this;
        }

        //q=from:germanf OR from:geferreyra
        public QueryBuilder WithFromThese(string from) {
            var arg = from.ToLower().Trim();
            if (!this.filter.FromThese.Contains(arg))
                this.filter.FromThese.Add(arg);

            return this;
        }

        public QueryBuilder WithToThese(string to) {
            var arg = to.ToLower().Trim();
            if (!this.filter.ToThese.Contains(arg))
                this.filter.ToThese.Add(arg);

            return this;
        }

        public QueryBuilder WithMentionThese(string mentioned) {
            var arg = mentioned.ToLower().Trim();
            if (!this.filter.MentionThese.Contains(arg))
                this.filter.MentionThese.Add(arg);

            return this;
        }

        public string Build() {
            var str = new StringBuilder();

            //public List<string> ExactWords { get; set; }
            //public string ExactPhrase { get; set; }
            //public List<string> AnyWord { get; set; }
            //public List<string> NoneWord { get; set; }
            //public List<string> HashTags { get; set; }
            //public List<string> FromThese { get; set; }
            //public List<string> ToThese { get; set; }
            //public List<string> MentionThese { get; set; }
            
            var mentions = string.Join(",", this.filter.MentionThese);
            str.Append(mentions);

            foreach (var phrase in this.filter.ExactPhrases) {
                str.AppendFormat("\"{0}\"", phrase);
            }

            return str.ToString();
        }

        protected QueryFilter filter { get; set; }
    }
}
