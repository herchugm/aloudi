using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class Dataset
    {
        public Dataset()
        {
            this.Dataset_Parameter = new List<Dataset_Parameter>();
            this.Views = new List<View>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public System.Guid Guid { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual ICollection<Dataset_Parameter> Dataset_Parameter { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<View> Views { get; set; }
    }
}
