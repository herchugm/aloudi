using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class User
    {
        public User()
        {
            this.Datasets = new List<Dataset>();
            this.Views = new List<View>();
            this.OAuthAccounts = new List<OAuthAccount>();
        }

        public int Id { get; set; }
        public System.Guid Guid { get; set; }
        public string Email { get; set; }
        public System.DateTime LastActivityDate { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public int Accesses { get; set; }
        public int RoleId { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual ICollection<Dataset> Datasets { get; set; }
        public virtual ICollection<View> Views { get; set; }
        public virtual Account Account { get; set; }
        public virtual ICollection<OAuthAccount> OAuthAccounts { get; set; }
    }
}
