using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class View
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DatasetId { get; set; }
        public System.Guid Guid { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual Dataset Dataset { get; set; }
        public virtual Layout Layout { get; set; }
        public virtual User User { get; set; }
    }
}
