using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class Field
    {
        public Field()
        {
            this.Dataset_Parameter = new List<Dataset_Parameter>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string API_Name { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual ICollection<Dataset_Parameter> Dataset_Parameter { get; set; }
    }
}
