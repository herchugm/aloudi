using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class Account
    {
        public int UserId { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> LastPasswordChangedDate { get; set; }
        public short PasswordFailuresSinceLastSuccess { get; set; }
        public Nullable<System.DateTime> LastPasswordFailureDate { get; set; }
        public bool IsConfirmed { get; set; }
        public string ConfirmationToken { get; set; }
        public bool IsLockedOut { get; set; }
        public Nullable<System.DateTime> LastLockoutDate { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual User User { get; set; }
    }
}
