using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class Layout
    {
        public int Id { get; set; }
        public string CSS { get; set; }
        public string JS { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual View View { get; set; }
    }
}
