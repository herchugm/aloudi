using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class Dataset_Parameter
    {
        public int Id { get; set; }
        public int DatasetId { get; set; }
        public int FieldId { get; set; }
        public string Value { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual Dataset Dataset { get; set; }
        public virtual Field Field { get; set; }
    }
}
