using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Piwer.Data.Mapping
{
    public class OAuthAccountMap : EntityTypeConfiguration<OAuthAccount>
    {
        public OAuthAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Source)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ClaimedIdentifier)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("OAuthAccount", "Users");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.ClaimedIdentifier).HasColumnName("ClaimedIdentifier");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Updated).HasColumnName("Updated");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.OAuthAccounts)
                .HasForeignKey(d => d.UserId);

        }
    }
}
