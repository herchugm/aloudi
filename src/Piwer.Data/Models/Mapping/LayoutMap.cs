using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Piwer.Data.Mapping
{
    public class LayoutMap : EntityTypeConfiguration<Layout>
    {
        public LayoutMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Layout");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CSS).HasColumnName("CSS");
            this.Property(t => t.JS).HasColumnName("JS");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Updated).HasColumnName("Updated");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            this.HasRequired(t => t.View)
                .WithOptional(t => t.Layout);

        }
    }
}
