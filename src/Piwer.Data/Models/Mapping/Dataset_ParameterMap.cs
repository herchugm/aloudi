using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Piwer.Data.Mapping
{
    public class Dataset_ParameterMap : EntityTypeConfiguration<Dataset_Parameter>
    {
        public Dataset_ParameterMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.RowVersion)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Dataset_Parameter");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DatasetId).HasColumnName("DatasetId");
            this.Property(t => t.FieldId).HasColumnName("FieldId");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Created).HasColumnName("Created");
            this.Property(t => t.Updated).HasColumnName("Updated");
            this.Property(t => t.RowVersion).HasColumnName("RowVersion");

            // Relationships
            this.HasRequired(t => t.Dataset)
                .WithMany(t => t.Dataset_Parameter)
                .HasForeignKey(d => d.DatasetId);
            this.HasRequired(t => t.Field)
                .WithMany(t => t.Dataset_Parameter)
                .HasForeignKey(d => d.FieldId);

        }
    }
}
