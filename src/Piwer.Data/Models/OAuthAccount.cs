using System;
using System.Collections.Generic;

namespace Piwer.Data
{
    public partial class OAuthAccount
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Source { get; set; }
        public string ClaimedIdentifier { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Updated { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual User User { get; set; }
    }
}
