using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Piwer.Data.Mapping;

namespace Piwer.Data
{
    public partial class PiwerContext : DbContext
    {
        static PiwerContext()
        {
            Database.SetInitializer<PiwerContext>(null);
        }

        public PiwerContext()
            : base("Name=piwerContext")
        {
        }

        public DbSet<Dataset> Datasets { get; set; }
        public DbSet<Dataset_Parameter> Dataset_Parameter { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<Layout> Layouts { get; set; }
        public DbSet<View> Views { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<OAuthAccount> OAuthAccounts { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DatasetMap());
            modelBuilder.Configurations.Add(new Dataset_ParameterMap());
            modelBuilder.Configurations.Add(new FieldMap());
            modelBuilder.Configurations.Add(new LayoutMap());
            modelBuilder.Configurations.Add(new ViewMap());
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new OAuthAccountMap());
            modelBuilder.Configurations.Add(new UserMap());
        }
    }
}
