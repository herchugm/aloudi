﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Piwer.ETL.Extractors.Twitter.Tests.Common;
using Piwer.ETL.Extractors.Twitter;

namespace Piwer.ETL.Extractors.Twitter.Tests {
    [TestClass]
    public class QueryBuilderTest : TestBase {
        [TestMethod()]
        public void WithSingleExactPhraseTest() {
            try {
                var query = string.Empty;
                var queryBuilder = new QueryBuilder();

                var single = "esta frase";
                queryBuilder.WithExactPhrase(single);
                query = queryBuilder.Build();
                var expected = string.Format("\"{0}\"", single);

                Assert.AreEqual(query, expected, "the phrase is wrong.");

            } catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
        }
    }
}
