﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Configuration;

namespace Piwer.ETL.Extractors.Twitter.Tests.Common {
    [TestClass]
    public class TestBase {
        protected static readonly string CONSUMER_KEY;
        protected static readonly string CONSUMER_SECRET;
        protected static readonly string ACCESS_TOKEN;
        protected static readonly string ACCESS_TOKEN_SECRET;

        static TestBase() {
            CONSUMER_KEY = ConfigurationManager.AppSettings["CONSUMER_KEY"];
            CONSUMER_SECRET = ConfigurationManager.AppSettings["CONSUMER_SECRET"];
            ACCESS_TOKEN = ConfigurationManager.AppSettings["ACCESS_TOKEN"];
            ACCESS_TOKEN_SECRET = ConfigurationManager.AppSettings["ACCESS_TOKEN_SECRET"];
        }
    }
}
