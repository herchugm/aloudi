﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Piwer.WebApp.Startup))]
namespace Piwer.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
